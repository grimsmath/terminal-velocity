﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TerminalVelocity.Account
{
    public partial class Register : System.Web.UI.Page
    {
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            String fname = this.tbxFirstName.Text;
            String lname = this.tbxLastName.Text;
            String email = this.tbxEmail.Text;
            String street = this.tbxStreet.Text;
            String city = this.ddlCity.Text;
            String state = this.ddlState.Text;
            String postal = this.tbxPostalCode.Text;
            String phone = this.tbxPhone.Text;
            String username = this.tbxUsername.Text;
            String password = this.tbxPassword.Text;

            String sqlCommand = "EXEC dbo.RegisterCustomer '" + fname +
                    "', '" + lname +
                    "', '" + email +
                    "', '" + phone +
                    "', '" + street +
                    "', '" + city +
                    "', '" + state +
                    "', '" + postal +
                    "', '" + username +
                    "', '" + password + "'";

            Database.ExecuteSql(sqlCommand);

            Response.Redirect("~/Account/Login.aspx");
        }
    }
}