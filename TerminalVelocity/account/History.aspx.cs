﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TerminalVelocity
{
    public partial class History : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void GridView1_Load(object sender, EventArgs e)
        {
            if (Session["customerID"] != null)
            {
                double dblValue = Database.GetBookingsTotal(Session["customerID"].ToString(), "1");
                this.lblTotal.Text += " $" + dblValue.ToString();
            }
            else
            {
                this.lblTotal.Text = "";
            }
        }    
    }
}