﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="Logout.aspx.cs" Inherits="TerminalVelocity.Account.Logout" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <div class="container_24">
        <div class="grid_5">&nbsp;</div>

        <div class="grid_14">
            <div class="center_box">
                <fieldset>
                    <legend>You Have Been Logged Out</legend>
                    <h1>Click&nbsp;<asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Default.aspx">here</asp:HyperLink> to return to the home page.</h1>
                </fieldset>
            </div>
        </div>

        <div class="grid_5">&nbsp;</div>
    </div>
</asp:Content>
