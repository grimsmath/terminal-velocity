﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="Cart.aspx.cs" Inherits="TerminalVelocity.Account.Cart" %>
<%@ MasterType VirtualPath="~/Site.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <div class="container_24 clearfix">
        <div class="grid_24">
            <div class="center_box">
                <div id="cart-table-container">
                    <fieldset>
                        <legend>Your Incomplete Bookings</legend>
                        <asp:GridView ID="GridView1" runat="server" AllowPaging="True" 
                            AutoGenerateColumns="False" DataKeyNames="Event ID" 
                            DataSourceID="SqlDataSource1" onload="GridView1_Load">
                            <Columns>
                                <asp:BoundField DataField="Booking ID" 
                                    SortExpression="Booking ID" HeaderText="Booking ID" />
                                <asp:BoundField DataField="Customer ID" HeaderText="Customer ID" 
                                    SortExpression="Customer ID" />
                                <asp:BoundField DataField="Event ID" HeaderText="Event ID" ReadOnly="True" 
                                    SortExpression="Event ID" />
                                <asp:BoundField DataField="Event Name" HeaderText="Event Name" 
                                    SortExpression="Event Name" />
                                <asp:BoundField DataField="Street Address" HeaderText="Street Address" 
                                    SortExpression="Street Address" />
                                <asp:BoundField DataField="City" HeaderText="City" 
                                    SortExpression="City" />
                                <asp:BoundField DataField="State" HeaderText="State" 
                                    SortExpression="State" />
                                <asp:BoundField DataField="Event Cost" HeaderText="Event Cost" 
                                    SortExpression="Event Cost" >
                                <ItemStyle HorizontalAlign="Right" />
                                </asp:BoundField>
                            </Columns>
                        </asp:GridView>
                        <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                            ConnectionString="<%$ ConnectionStrings:cop3855fall11TerminalVelocityConnectionString %>" 
                            
                            SelectCommand="SELECT * FROM [VIEW_OPEN_BOOKINGS] WHERE ([Customer ID] = @Customer_ID) ORDER BY [Booking ID]">
                            <SelectParameters>
                                <asp:SessionParameter Name="Customer_ID" SessionField="customerID" 
                                    Type="Int32" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </fieldset>

                    <fieldset>
                        <legend>Booking(s) Totals</legend>
                        <h1>
                            <asp:Label ID="lblTotal" runat="server" Text="Your outstanding bookings total:"></asp:Label>
                        </h1>
                    </fieldset>
                    
                    <fieldset>
                        <asp:Button ID="btnComplete" runat="server" Text="Complete Bookings" 
                            CssClass="cart-complete-button" onclick="btnComplete_Click" />
                    </fieldset>

                    <fieldset>
                        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Account/History.aspx">View Your Booking History</asp:HyperLink>
                    </fieldset>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
