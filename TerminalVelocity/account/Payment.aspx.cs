﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TerminalVelocity.Account
{
    public partial class Payment : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void ddlYear_Load(object sender, EventArgs e)
        {
            for (int i = DateTime.Today.Year; i < (DateTime.Today.Year + 20); i++)
            {
                this.ddlYear.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (Session["customerID"] != null)
            {
                int expireMonth, expireYear;

                int.TryParse(this.ddlMonth.SelectedValue, out expireMonth);
                int.TryParse(this.ddlYear.SelectedValue, out expireYear);

                Database.SubmitPayment(Session["customerID"].ToString(),
                    this.ddlCardType.SelectedValue,
                    this.tbxCardName.Text,
                    this.tbxCardNumber.Text,
                    expireMonth,
                    expireYear,
                    this.tbxSecurityCode.Text);

                this.pnlNewPayment.Visible = false;
                this.pnlConfirmation.Visible = true;
            }
            else
            {
                Response.Redirect("~/Account/Login.aspx");
            }
        }
    }
}