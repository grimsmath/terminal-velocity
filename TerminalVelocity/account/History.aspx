﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="History.aspx.cs" Inherits="TerminalVelocity.History" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <div class="container_24">
        <div class="grid_24">
            <div class="center_box">
                <div id="cart-table-container">
                    <fieldset>
                        <legend>Your Booking History</legend>
                        <asp:GridView ID="gvHistory" runat="server">
                        </asp:GridView>
                        <asp:GridView ID="GridView1" runat="server" AllowPaging="True" 
                            AutoGenerateColumns="False" DataKeyNames="Event ID" 
                            DataSourceID="SqlDataSource1" onload="GridView1_Load">
                            <Columns>
                                <asp:BoundField DataField="Booking ID" 
                                    SortExpression="Booking ID" HeaderText="Booking ID" Visible="False" />
                                <asp:BoundField DataField="Booking Date" 
                                    SortExpression="Booking Date" HeaderText="Booking Date " />
                                <asp:BoundField DataField="Customer ID" HeaderText="Customer ID" 
                                    SortExpression="Customer ID" Visible="False" />
                                <asp:BoundField DataField="Event ID" HeaderText="Event ID" ReadOnly="True" 
                                    SortExpression="Event ID" Visible="False" />
                                <asp:BoundField DataField="Event Name" HeaderText="Event Name" 
                                    SortExpression="Event Name" />
                                <asp:BoundField DataField="Street Address" HeaderText="Street Address" 
                                    SortExpression="Street Address" />
                                <asp:BoundField DataField="City" HeaderText="City" 
                                    SortExpression="City" />
                                <asp:BoundField DataField="State" HeaderText="State" 
                                    SortExpression="State" />
                                <asp:BoundField DataField="Event Cost" HeaderText="Event Cost" 
                                    SortExpression="Event Cost" >
                                <ItemStyle HorizontalAlign="Right" />
                                </asp:BoundField>
                            </Columns>
                        </asp:GridView>
                        <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                            ConnectionString="<%$ ConnectionStrings:cop3855fall11TerminalVelocityConnectionString %>" 
                            
                            SelectCommand="SELECT * FROM [VIEW_CLOSED_BOOKINGS] WHERE ([Customer ID] = @Customer_ID) ORDER BY [Booking ID]">
                            <SelectParameters>
                                <asp:SessionParameter Name="Customer_ID" SessionField="customerID" 
                                    Type="Int32" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </fieldset>
                    <fieldset>
                        <legend>Booking(s) Totals</legend>
                        <h1>
                            <asp:Label ID="lblTotal" runat="server" Text="Your completed bookings total:"></asp:Label>
                        </h1>
                    </fieldset>
                    <fieldset>
                        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Account/Cart.aspx">View Your Incomplete Bookings</asp:HyperLink>
                    </fieldset>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
