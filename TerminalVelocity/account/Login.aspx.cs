﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace TerminalVelocity.Account
{
	public partial class Login : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			this.Master.SetActiveTab("lnkMenuLogin");
		}

		protected void lnkRegister_Click(object sender, EventArgs e)
		{
			Response.Redirect("~/Account/Register.aspx");
		}

		protected void AccountLogin_Authenticate(object sender, AuthenticateEventArgs e)
		{
			String username = this.AccountLogin.UserName;
			String password = this.AccountLogin.Password;

			if (Database.AuthenticateUser(username, password))
			{
				e.Authenticated = true;
				
				Session["username"] = username;
				Session["customerID"] = Database.GetCustomerID(Session["username"].ToString());
			}
			else
			{
				e.Authenticated = false;
			}
		}

		protected void AccountLogin_LoggedIn(object sender, EventArgs e)
		{
			Response.Redirect("~/Account/Cart.aspx");
		}
	}
}