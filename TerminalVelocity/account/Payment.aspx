﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="Payment.aspx.cs" Inherits="TerminalVelocity.Account.Payment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="container_24">
                <div class="grid_5">
                    &nbsp;</div>
                <div class="grid_14">
                    <div class="grid_12 center_box">
                        <asp:Panel ID="pnlNewPayment" runat="server">
                            <fieldset>
                                <legend>Please supply the following</legend>
                                <table border="0" width="100%">
                                    <tr>
                                        <td>
                                            Credit/Debit Card Type:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlCardType" runat="server">
                                                <asp:ListItem Value="VISA">VISA</asp:ListItem>
                                                <asp:ListItem Value="MasterCard">MasterCard</asp:ListItem>
                                                <asp:ListItem Value="Discover">Discover</asp:ListItem>
                                                <asp:ListItem Value="Diner's Club">Diner's Club</asp:ListItem>
                                                <asp:ListItem Value="Other">Other</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Name As It Appears On Card:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbxCardName" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Card Number:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbxCardNumber" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Expiration Month:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlMonth" runat="server">
                                                <asp:ListItem Value="01">01 - January</asp:ListItem>
                                                <asp:ListItem Value="02">02 - February</asp:ListItem>
                                                <asp:ListItem Value="03">03 - March</asp:ListItem>
                                                <asp:ListItem Value="04">04 - April</asp:ListItem>
                                                <asp:ListItem Value="05">05 - May</asp:ListItem>
                                                <asp:ListItem Value="05">06 - June</asp:ListItem>
                                                <asp:ListItem Value="05">07 - July</asp:ListItem>
                                                <asp:ListItem Value="05">08 - August</asp:ListItem>
                                                <asp:ListItem Value="05">09 - September</asp:ListItem>
                                                <asp:ListItem Value="05">10 - October</asp:ListItem>
                                                <asp:ListItem Value="05">11 - November</asp:ListItem>
                                                <asp:ListItem Value="05">12 - December</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Expiration Year:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlYear" runat="server" OnLoad="ddlYear_Load">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Security Code:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbxSecurityCode" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>
                            <fieldset>
                                <asp:Button ID="btnSubmit" runat="server" Text="Submit Payment" OnClick="btnSubmit_Click"
                                    CssClass="cart-complete-button" />
                            </fieldset>
                            <fieldset>
                                <ul>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="<li>Please enter the name on the credit card.</li>"
                                        ControlToValidate="tbxCardName" Display="Dynamic"></asp:RequiredFieldValidator>

                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="<li>Please enter a valid credit card number.</li>"
                                        ControlToValidate="tbxCardNumber" Display="Dynamic"></asp:RequiredFieldValidator>

                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="tbxCardNumber"
                                        ErrorMessage="<li>Please enter a valid credit card number (dashes optional).</li>"                                        
                                        
                                        ValidationExpression="^((4\d{3})|(5[1-5]\d{2})|(6011))-?\d{4}-?\d{4}-?\d{4}|3[4,7]\d{13}$" 
                                        Display="Dynamic"></asp:RegularExpressionValidator>

                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="tbxSecurityCode"
                                        ErrorMessage="<li>Please enter a security code.</li>" Display="Dynamic"></asp:RequiredFieldValidator>
                                </ul>
                            </fieldset>

                        </asp:Panel>
                        <asp:Panel ID="pnlConfirmation" runat="server" Visible="False">
                            <fieldset>
                                <legend>Thank Your For Completing Your Bookings!</legend>
                                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Default.aspx">Click here to return to the search page.</asp:HyperLink>
                            </fieldset>
                        </asp:Panel>
                    </div>
                </div>
            </div>
            <div class="grid_5">
                &nbsp;</div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
