﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Configuration;
using System.Data.SqlClient;

namespace TerminalVelocity.Account
{
    public partial class Cart : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Master.SetActiveTab("lnkMenuCart");

            if (Session["customerID"] == null)
            {
                Response.Redirect("~/Account/Login.aspx");
            }
            else
            {
                if (this.GridView1.Rows.Count > 0)
                {
                    if (Page.Request.QueryString["action"] != null &&
                        Page.Request.QueryString["action"].ToString() == "BookEvent")
                    {
                        String eventID = Page.Request.QueryString["eventID"];
                        String custID = Session["customerID"].ToString();

                        // Create the booking
                        String sql = "EXEC dbo.BookEvent '" + eventID + "', '" + custID + "', '" + DateTime.Today + "'";
                        Database.ExecuteSql(sql);
                    }
                }
                else
                {
                    this.btnComplete.Enabled = false;
                }
            }
        }

        protected void GridView1_Load(object sender, EventArgs e)
        {
            if (Session["customerID"] != null)
            {
                double dblValue = Database.GetBookingsTotal(Session["customerID"].ToString(), "0");
                this.lblTotal.Text += " $" + dblValue.ToString();
            }
            else
            {
                this.lblTotal.Text = "";
            }
        }

        protected void btnComplete_Click(object sender, EventArgs e)
        {
            if (this.GridView1.Rows.Count > 0 && Session["customerID"] != null)
            {
                Response.Redirect("~/Account/Payment.aspx?customerID=" + Session["customerID"].ToString());
            }
        }
    }
}