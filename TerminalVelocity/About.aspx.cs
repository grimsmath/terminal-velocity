﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TerminalVelocity
{
    public partial class Explore : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Master.SetActiveTab("lnkMenuAbout");
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            this.pnlManagement.Visible = true;
            this.pnlMission.Visible = false;
            this.pnlTechnology.Visible = false;
            this.pnlFAQ.Visible = false;
            this.pnlShipping.Visible = false;
            this.pnlReturns.Visible = false;
            this.pnlPrivacy.Visible = false;
        }

        protected void LinkButton2_Click(object sender, EventArgs e)
        {
            this.pnlManagement.Visible = false;
            this.pnlMission.Visible = true;
            this.pnlTechnology.Visible = false;
            this.pnlShipping.Visible = false;
            this.pnlReturns.Visible = false;
            this.pnlFAQ.Visible = false;
            this.pnlPrivacy.Visible = false;
        }

        protected void LinkButton3_Click(object sender, EventArgs e)
        {
            this.pnlManagement.Visible = false;
            this.pnlMission.Visible = false;
            this.pnlTechnology.Visible = true;
            this.pnlShipping.Visible = false;
            this.pnlReturns.Visible = false;
            this.pnlFAQ.Visible = false;
            this.pnlPrivacy.Visible = false;
        }

        protected void LinkButton4_Click(object sender, EventArgs e)
        {
            this.pnlManagement.Visible = false;
            this.pnlMission.Visible = false;
            this.pnlTechnology.Visible = false;
            this.pnlShipping.Visible = false;
            this.pnlReturns.Visible = false;
            this.pnlFAQ.Visible = true;
            this.pnlPrivacy.Visible = false;
        }

        protected void LinkButton6_Click(object sender, EventArgs e)
        {
            this.pnlManagement.Visible = false;
            this.pnlMission.Visible = false;
            this.pnlTechnology.Visible = false;
            this.pnlFAQ.Visible = false;
            this.pnlShipping.Visible = false;
            this.pnlReturns.Visible = false;
            this.pnlPrivacy.Visible = true;
        }

        protected void LinkButton8_Click(object sender, EventArgs e)
        {
            this.pnlManagement.Visible = false;
            this.pnlMission.Visible = false;
            this.pnlTechnology.Visible = false;
            this.pnlFAQ.Visible = false;
            this.pnlShipping.Visible = true;
            this.pnlReturns.Visible = false;
            this.pnlPrivacy.Visible = false;
        }

        protected void LinkButton7_Click(object sender, EventArgs e)
        {
            this.pnlManagement.Visible = false;
            this.pnlMission.Visible = false;
            this.pnlTechnology.Visible = false;
            this.pnlFAQ.Visible = false;
            this.pnlShipping.Visible = false;
            this.pnlReturns.Visible = true;
            this.pnlPrivacy.Visible = false;
        }
    }
}