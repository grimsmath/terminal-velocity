﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="About.aspx.cs" Inherits="TerminalVelocity.Explore" %>

<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register TagPrefix="uc" TagName="Policies" Src="~/Controls/Policies.ascx" %>
<%@ Register TagPrefix="uc" TagName="FAQ" Src="~/Controls/FAQ.ascx" %>
<%@ MasterType VirtualPath="~/Site.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="Server" />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="grid_8">
                <div class="info_box round_box link_box">
                    <h1>
                        Important Links About Us</h1>
                    <ul>
                        <li>
                            <asp:LinkButton ID="LinkButton1" runat="server" OnClick="LinkButton1_Click">Our Management Team</asp:LinkButton></li>
                        <li>
                            <asp:LinkButton ID="LinkButton2" runat="server" OnClick="LinkButton2_Click">Our Mission</asp:LinkButton></li>
                        <li>
                            <asp:LinkButton ID="LinkButton3" runat="server" OnClick="LinkButton3_Click">Our Technology</asp:LinkButton></li>
                        <li>
                            <asp:LinkButton ID="LinkButton6" runat="server" OnClick="LinkButton6_Click">Our Privacy Policy</asp:LinkButton></li>
                        <li>
                            <asp:LinkButton ID="LinkButton8" runat="server" OnClick="LinkButton8_Click">Shipping Policy</asp:LinkButton></li>
                        <li>
                            <asp:LinkButton ID="LinkButton7" runat="server" OnClick="LinkButton7_Click">Returns &amp; Exchanges</asp:LinkButton></li>
                        <li>
                            <asp:LinkButton ID="LinkButton5" runat="server" OnClick="LinkButton4_Click">Frequently Asked Questions</asp:LinkButton></li>
                    </ul>
                </div>
            </div>
            <div class="grid_16">
                <div class="info_box round_box">
                    <asp:Panel ID="pnlManagement" runat="server">
                        <h1>
                            Our Management Teacm</h1>
                        <asp:Accordion ID="accordionQuestions" runat="server" SelectedIndex="0" HeaderCssClass="accordionHeader"
                            HeaderSelectedCssClass="accordionHeaderSelected" ContentCssClass="accordionContent"
                            FadeTransitions="false" FramesPerSecond="40" TransitionDuration="250" AutoSize="None"
                            RequireOpenedPane="false" SuppressHeaderPostbacks="true">
                            <Panes>
                                <asp:AccordionPane ID="AccordionPane1" runat="server">
                                    <Header>
                                        <a href="#" class="accordionLink">B. R. Gary, Chief Quality Assurance and Marketing
                                            Officer</a>
                                    </Header>
                                    <Content>
                                        <p>
                                            After serving in the Navy for 11 years, B.R. Gary decided to pursue becoming his
                                            true love, being an extremophile. B.R. being our Chief Executive Office and Extreme
                                            Sportsphile, goes and tests many of the activities and sports our company allows
                                            you to search from. Everything from booking them on the site to BASE Jumping to
                                            writing a few of the reviews, he may be our biggest customer. He goes skydiving
                                            monthly constantly trying out new jump zones, and loves to try and find new sports
                                            and activities to participate in.</p>
                                    </Content>
                                </asp:AccordionPane>
                                <asp:AccordionPane ID="AccordionPane2" runat="server">
                                    <Header>
                                        <a href="#" class="accordionLink">Carly Pellegrini, Chief Financial Officer</a>
                                    </Header>
                                    <Content>
                                        <p>
                                            Carly Pellegrini was a child prodigy. She graduated with her PH.D. at 20 and worked
                                            for Kayak for 3 years. She specializes in Marketing and is capable of selling swampland
                                            to a Floridian and ice to an Eskimo. She enjoys traveling and white-water rafting
                                            all over the world.
                                        </p>
                                    </Content>
                                </asp:AccordionPane>
                                <asp:AccordionPane ID="AccordionPane3" runat="server">
                                    <Header>
                                        <a href="#" class="accordionLink">David King, Jr., Chief Technology Officer and Information
                                            Evangelist</a>
                                    </Header>
                                    <Content>
                                        <p>
                                            David is a dedicated technologist with almost 20 years of experience in the field
                                            of software engineering. He formerly worked as an Applications Systems Analyst and
                                            Software Engineer for YUM! Brands, Inc., a Fortune 500 restaurant management company.
                                            David is dedicated to working on the bleeding edge of technology and it is his engineering
                                            and software development talents that brings the online experience of Terminal Velocity
                                            to you.</p>
                                    </Content>
                                </asp:AccordionPane>
                            </Panes>
                        </asp:Accordion>
                    </asp:Panel>
                    <asp:Panel ID="pnlMission" runat="server" Visible="False">
                        <h1>
                            Our Mission</h1>
                        <p>
                            Terminal Velocity provides a service that will make travel arrangements for the
                            customer in the fields of extreme sports, airline flights, hotels and rental cars.
                            Customers can choose which of the above they want to book, or book packages of all
                            or some of the above. Our mission is to strive to be the best company in the field
                            to offer all of these amenities, in the easiest fashion for the customer.
                        </p>
                    </asp:Panel>
                    <asp:Panel ID="pnlTechnology" runat="server" Visible="False">
                        <h1>
                            Our Technology</h1>
                        <p>
                            We utilize Microsoft's ASP.Net and SQL Server technology, along with the ASP.Net
                            AJAX Toolkit and the jQuery JavaScript Library
                        </p>
                    </asp:Panel>
                    <asp:Panel ID="pnlPrivacy" runat="server" Visible="False">
                        <h1>
                            Privacy Policy</h1>
                        <div class="privacy">
                            <h2>
                                What information do we collect?</h2>
                            <p>
                                We collect information from you when you register on our site, place an order or
                                fill out a form.
                            </p>
                            <p>
                                When ordering or registering on our site, as appropriate, you may be asked to enter
                                your: name, e-mail address, mailing address, phone number or credit card information.
                                You may, however, visit our site anonymously.
                            </p>
                            <p>
                                Google, as a third party vendor, uses cookies to serve ads on your site. Google's
                                use of the DART cookie enables it to serve ads to your users based on their visit
                                to your sites and other sites on the Internet. Users may opt out of the use of the
                                DART cookie by visiting the Google ad and content network privacy policy..<br />
                            </p>
                            <h2>
                                What do we use your information for?</h2>
                            <p>
                                Any of the information we collect from you may be used in one of the following ways:
                            </p>
                            <ul>
                                <li>To personalize your experience (your information helps us to better respond to your
                                    individual needs)</li>
                                <li>To improve our website (we continually strive to improve our website offerings based
                                    on the information and feedback we receive from you)</li>
                                <li>To improve customer service (your information helps us to more effectively respond
                                    to your customer service requests and support needs)</li>
                                <li>To process transactions<blockquote>
                                    Your information, whether public or private, will not be sold, exchanged, transferred,
                                    or given to any other company for any reason whatsoever, without your consent, other
                                    than for the express purpose of delivering the purchased product or service requested.</blockquote>
                                </li>
                                <li>To administer a contest, promotion, survey or other site feature</li>
                                <li>To send periodic emails
                                    <blockquote>
                                        The email address you provide for order processing, will only be used to send you
                                        information and updates pertaining to your order.</blockquote>
                                </li>
                            </ul>
                            <h2>
                                How do we protect your information?</h2>
                            <p>
                                We implement a variety of security measures to maintain the safety of your personal
                                information when you place an order or enter, submit, or access your personal information.
                            </p>
                            <p>
                                We offer the use of a secure server. All supplied sensitive/credit information is
                                transmitted via Secure Socket Layer (SSL) technology and then encrypted into our
                                Database to be only accessed by those authorized with special access rights to our
                                systems, and are required to?keep the information confidential.
                            </p>
                            <p>
                                After a transaction, your private information (credit cards, social security numbers,
                                financials, etc.) will not be kept on file for more than 60 days.<br />
                            </p>
                            <h2>
                                Do we use cookies?</h2>
                            <p>
                                Yes (Cookies are small files that a site or its service provider transfers to your
                                computers hard drive through your Web browser (if you allow) that enables the sites
                                or service providers systems to recognize your browser and capture and remember
                                certain information<br />
                            </p>
                            <p>
                                We use cookies to help us remember and process the items in your shopping cart,
                                understand and save your preferences for future visits, keep track of advertisements
                                and compile aggregate data about site traffic and site interaction so that we can
                                offer better site experiences and tools in the future. We may contract with third-party
                                service providers to assist us in better understanding our site visitors. These
                                service providers are not permitted to use the information collected on our behalf
                                except to help us conduct and improve our business.<br />
                            </p>
                            <h2>
                                Do we disclose any information to outside parties?</h2>
                            <p>
                                We do not sell, trade, or otherwise transfer to outside parties your personally
                                identifiable information. This does not include trusted third parties who assist
                                us in operating our website, conducting our business, or servicing you, so long
                                as those parties agree to keep this information confidential. We may also release
                                your information when we believe release is appropriate to comply with the law,
                                enforce our site policies, or protect ours or others rights, property, or safety.
                                However, non-personally identifiable visitor information may be provided to other
                                parties for marketing, advertising, or other uses.<br />
                            </p>
                            <h2>
                                Third party links</h2>
                            <p>
                                Occasionally, at our discretion, we may include or offer third party products or
                                services on our website. These third party sites have separate and independent privacy
                                policies. We therefore have no responsibility or liability for the content and activities
                                of these linked sites. Nonetheless, we seek to protect the integrity of our site
                                and welcome any feedback about these sites.<br />
                            </p>
                            <h2>
                                California Online Privacy Protection Act Compliance</h2>
                            <p>
                                Because we value your privacy we have taken the necessary precautions to be in compliance
                                with the California Online Privacy Protection Act. We therefore will not distribute
                                your personal information to outside parties without your consent.<br />
                            </p>
                            <p>
                                As part of the California Online Privacy Protection Act, all users of our site may
                                make any changes to their information at anytime by logging into their control panel
                                and going to the 'Edit Profile' page.<br />
                            </p>
                            <h2>
                                Childrens Online Privacy Protection Act Compliance</h2>
                            <p>
                                We are in compliance with the requirements of COPPA (Childrens Online Privacy Protection
                                Act), we do not collect any information from anyone under 13 years of age. Our website,
                                products and services are all directed to people who are at least 13 years old or
                                older.
                            </p>
                            <h2>
                                Online Privacy Policy Only</h2>
                            <p>
                                This online privacy policy applies only to information collected through our website
                                and not to information collected offline.<br />
                            </p>
                            <h2>
                                Your Consent</h2>
                            <p>
                                By using our site, you consent to our <a style='text-decoration: none; color: #3C3C3C;'
                                    href='http://www.terminalvelocity.us/Account.aspx' target='_blank'>websites privacy
                                    policy</a>.<br />
                            </p>
                            <h2>
                                Changes to our Privacy Policy</h2>
                            <p>
                                If we decide to change our privacy policy, we will post those changes on this page,
                                and/or send an email notifying you of any changes.
                            </p>
                    </asp:Panel>
                    <asp:Panel ID="pnlShipping" runat="server" Visible="False">
                        <h1>Shipping of Tickets &amp; Other Products</h1>
                        <p>
                            All tickets purchased will be shipped via First Class Mail via the United States Postal Service.
                        </p>
                    </asp:Panel>
                    <asp:Panel ID="pnlReturns" runat="server" Visible="False">
                        <h1>Returns &amp; Exchanges</h1>
                        <p>
                            All tickets and bookings must be canceled at least 48 hours prior to the event to receive a full refund.
                            Refunds will be made to the credit or debit card used to make the purchase.  Please contact Terminal Velocity at
                            904-555-1212 to speak to a customer service representative to cancel your purchase.
                        </p>
                    </asp:Panel>
                    <asp:Panel ID="pnlFAQ" runat="server" Visible="False">
                        <uc:FAQ ID="Questions" runat="server" />
                    </asp:Panel>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
