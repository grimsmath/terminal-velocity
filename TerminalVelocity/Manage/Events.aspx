﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Events.aspx.cs" Inherits="TerminalVelocity.Manage.Events" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <div class="grid_5">
    
    </div>

    <div class="grid_14">
        <asp:GridView ID="gvEvents" runat="server" AutoGenerateColumns="False" 
            DataSourceID="SqlDataSource1">
            <Columns>
                <asp:BoundField DataField="Event Name" HeaderText="Event Name" 
                    SortExpression="Event Name" />
                <asp:BoundField DataField="Event Type" HeaderText="Event Type" 
                    SortExpression="Event Type" />
                <asp:BoundField DataField="Event Contact" HeaderText="Event Contact" 
                    SortExpression="Event Contact" />
                <asp:BoundField DataField="Street" HeaderText="Street" 
                    SortExpression="Street" />
                <asp:BoundField DataField="City" HeaderText="City" SortExpression="City" />
                <asp:BoundField DataField="State" HeaderText="State" SortExpression="State" />
                <asp:BoundField DataField="Available" HeaderText="Available" 
                    SortExpression="Available" />
                <asp:BoundField DataField="Expires" HeaderText="Expires" 
                    SortExpression="Expires" />
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConnectionString="<%$ ConnectionStrings:cop3855fall11TerminalVelocityConnectionString %>" 
            SelectCommand="SELECT * FROM [VIEW_EVENTS]"></asp:SqlDataSource>
    </div>

    <div class="grid_5">
    
    </div>
</asp:Content>
