﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace TerminalVelocity
{
	public partial class SiteMaster : System.Web.UI.MasterPage
	{
        public string BasePath
        {
            get
            {
                return string.Format("http://{0}{1}",
                               HttpContext.Current.Request.ServerVariables["HTTP_HOST"],
                               (VirtualFolder.Equals("/")) ? string.Empty : VirtualFolder);
            }
        }

		public string Username
		{
			get
			{
				return this.lblUsername.Text;
			}
			set
			{
				this.lblUsername.Text = value;
			}
		}

        private static string VirtualFolder
        {
            get { return HttpContext.Current.Request.ApplicationPath; }
        }

        public void SetActiveTab(String tabName)
        {
            if (tabName == "lnkMenuSearch")
            {
                this.lnkMenuSearch.CssClass = "active";
                this.lnkMenuBrowse.CssClass = "";
                this.lnkMenuAbout.CssClass = "";
                this.lnkMenuCart.CssClass = "";
                this.lnkMenuLogin.CssClass = "";
            }
            else if (tabName == "lnkMenuBrowse")
            {
                this.lnkMenuSearch.CssClass = "";
                this.lnkMenuBrowse.CssClass = "active";
                this.lnkMenuAbout.CssClass = "";
                this.lnkMenuCart.CssClass = "";
                this.lnkMenuLogin.CssClass = "";
            }
            else if (tabName == "lnkMenuAbout")
            {
                this.lnkMenuSearch.CssClass = "";
                this.lnkMenuBrowse.CssClass = "";
                this.lnkMenuAbout.CssClass = "active";
                this.lnkMenuCart.CssClass = "";
                this.lnkMenuLogin.CssClass = "";
            }
            else if (tabName == "lnkMenuCart")
            {
                this.lnkMenuSearch.CssClass = "";
                this.lnkMenuBrowse.CssClass = "";
                this.lnkMenuAbout.CssClass = "";
                this.lnkMenuCart.CssClass = "active";
                this.lnkMenuLogin.CssClass = "";
            }
            else if (tabName == "lnkMenuLogin")
            {
                this.lnkMenuSearch.CssClass = "";
                this.lnkMenuBrowse.CssClass = "";
                this.lnkMenuAbout.CssClass = "";
                this.lnkMenuCart.CssClass = "";
                this.lnkMenuLogin.CssClass = "active";
            }
        }

		protected void Page_Load(object sender, EventArgs e)
		{
            Page.Header.DataBind();

            if (Session["username"] != null && Session["username"].ToString() != "")
            {
                this.Username = Session["username"].ToString();
                this.lnkMenuLogin.Text = "Logout";
                this.lnkMenuLogin.NavigateUrl = "~/Account/Logout.aspx";
                this.lnkMenuCart.Visible = true;
            }
            else
            {
                this.Username = "Guest";
                this.lnkMenuLogin.Text = "Login";
                this.lnkMenuLogin.NavigateUrl = "~/Account/Login.aspx";
                this.lnkMenuCart.Visible = false;
            }
        }
	}
}
