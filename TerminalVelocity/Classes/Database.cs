﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;

namespace TerminalVelocity
{
    public class Database
    {
        private static String connectionString = "cop3855fall11TerminalVelocityConnectionString";

        public static String GetCustomerID(String username)
        {
            String customerID = "";
            String conStr = WebConfigurationManager.ConnectionStrings[connectionString].ConnectionString;

            //Create a SqlConnection to the Northwind database.
            using (SqlConnection connection = new SqlConnection(conStr))
            {
                connection.Open();

                String sql = "SELECT customerID FROM dbo.tv_Customer WHERE dbo.tv_Customer.username = '" + username + "'";
                SqlCommand cmd = new SqlCommand(sql);

                cmd.Connection = connection;
                SqlDataReader rd = cmd.ExecuteReader();
                rd.Read();

                customerID = rd["customerID"].ToString();
            }

            return customerID;
        }

        public static void SaveFeedback(String custID, String feedback, String tripDate)
        {
            String conStr = WebConfigurationManager.ConnectionStrings[connectionString].ConnectionString;

            //Create a SqlConnection to the Northwind database.
            using (SqlConnection connection = new SqlConnection(conStr))
            {
                connection.Open();

                String sql = "EXEC dbo.SaveFeedback '" + custID + "', '" + feedback + "', '" + tripDate + "'";
                SqlCommand cmd = new SqlCommand(sql);

                cmd.Connection = connection;
                cmd.ExecuteNonQuery();
            }
        }

        public static void SubmitPayment(String customerID, 
                                            String nameOnCard,
                                            String cardType, 
                                            String cardNumber, 
                                            int expireMonth, 
                                            int expireYear, 
                                            String securityCode)
        {
            String conStr = WebConfigurationManager.ConnectionStrings["cop3855fall11TerminalVelocityConnectionString"].ConnectionString;

            //Create a SqlConnection to the Northwind database.
            using (SqlConnection connection = new SqlConnection(conStr))
            {
                connection.Open();

                String sql = "EXEC dbo.SubmitPayment '" + 
                    customerID      + "', '" +
                    nameOnCard      + "', '" +
                    cardType        + "', '" +
                    cardNumber      + "', '" +
                    expireMonth     + "', '" +
                    expireYear      + "', '" +
                    securityCode    + "'";

                SqlCommand cmd = new SqlCommand(sql);

                cmd.Connection = connection;
                cmd.ExecuteNonQuery();
            }
        }

        public static double GetBookingsTotal(String customerID, string statusCode)
        {
            double dblTotal = 0.0;
            String conStr = WebConfigurationManager.ConnectionStrings["cop3855fall11TerminalVelocityConnectionString"].ConnectionString;

            //Create a SqlConnection to the Northwind database.
            using (SqlConnection connection = new SqlConnection(conStr))
            {
                connection.Open();

                String sql = "SELECT SUM(eventCost) AS Total FROM dbo.VIEW_BOOKINGS " +
                    "WHERE dbo.VIEW_BOOKINGS.customerID='" + customerID + "' AND bookingStatus='" + statusCode + "'";

                SqlCommand cmd = new SqlCommand(sql);

                cmd.Connection = connection;
                SqlDataReader rd = cmd.ExecuteReader();
                rd.Read();

                Double.TryParse(rd["Total"].ToString(), out dblTotal);
            }

            return dblTotal;
        }

        public static void ExecuteSql(String sqlCommand)
        {
            String conStr = WebConfigurationManager.ConnectionStrings[connectionString].ConnectionString;

            //Create a SqlConnection to the Northwind database.
            using (SqlConnection connection = new SqlConnection(conStr))
            {
                connection.Open();
                
                SqlCommand cmd = new SqlCommand();

                cmd.CommandText = sqlCommand;
                cmd.Connection = connection;
                cmd.ExecuteNonQuery();
            }
        }

        public static bool AuthenticateUser(String username, String password)
        {
            bool bReturn = false;
            String conStr = WebConfigurationManager.ConnectionStrings[connectionString].ConnectionString;

            //Create a SqlConnection to the Northwind database.
            using (SqlConnection connection = new SqlConnection(conStr))
            {
                connection.Open();
                String sql = "SELECT * FROM tv_Customer WHERE username='" + username + "' AND password='" + password + "'";
                SqlCommand cmd = new SqlCommand(sql);
                cmd.Connection = connection;
                SqlDataReader rd = cmd.ExecuteReader();
                if (rd.HasRows)
                    bReturn = true;
            }

            return bReturn;
        }

        public static DataSet GetDataSet(String sqlCommand, String dataSetName)
        {
            String conStr = WebConfigurationManager.ConnectionStrings[connectionString].ConnectionString;
            DataSet dataSet = new DataSet(dataSetName);

            //Create a SqlConnection to the Northwind database.
            using (SqlConnection connection = new SqlConnection(conStr))
            {
                //Create a SqlDataAdapter for the Suppliers table.
                SqlDataAdapter adapter = new SqlDataAdapter();

                adapter.TableMappings.Add("Table", dataSetName);

                // Open the connection.
                connection.Open();

                // Initialize a SQL command object
                SqlCommand command = new SqlCommand(sqlCommand, connection);
                command.CommandType = CommandType.Text;

                // Set the SqlDataAdapter's SelectCommand.
                adapter.SelectCommand = command;

                // Fill the DataSet.
                adapter.Fill(dataSet);

                // Close the connection.
                connection.Close();
            }

            return dataSet;
        }
    }
}