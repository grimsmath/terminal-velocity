﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="Default.aspx.cs" Inherits="TerminalVelocity._Default" %>

<%@ MasterType VirtualPath="~/Site.master" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="Server" />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="container_24 clearfix">
                <div id="quicklinks-box" class="grid_8">
                    <div id="quicklinks" class="round_box">
                        <h1>Latest Reviews</h1>
                        <asp:SqlDataSource ID="SqlDataSource5" runat="server" 
                            ConnectionString="<%$ ConnectionStrings:cop3855fall11TerminalVelocityConnectionString %>" 
                            SelectCommand="SELECT * FROM [VIEW_FEEDBACK]"></asp:SqlDataSource>
                        <asp:Accordion ID="News" runat="server" SelectedIndex="0" HeaderCssClass="accordionHeader"
                            HeaderSelectedCssClass="accordionHeaderSelected" ContentCssClass="accordionContent"
                            FadeTransitions="false" FramesPerSecond="40" TransitionDuration="250" AutoSize="None"
                            RequireOpenedPane="false" SuppressHeaderPostbacks="true">
                            <ContentTemplate>
                                <asp:Repeater ID="Repeater1" runat="server" DataSourceID="SqlDataSource5">
                                    <ItemTemplate>
                                        <%# DataBinder.Eval(Container.DataItem, "eventDate") %>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </ContentTemplate>
                        </asp:Accordion>
                    </div>
                </div>

                <div class="grid_16">
                    <div class="center_box">
                        <fieldset>
                            <legend>What Type Of Extreme Sport Are You Interested In?</legend>
                            <div class="row">
                                <div class="col">
                                    <asp:DropDownList ID="ddlEventType" runat="server" DataSourceID="SqlDataSource1"
                                        DataTextField="eventTypeName" DataValueField="eventTypeName" AutoPostBack="True">
                                    </asp:DropDownList>
                                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:cop3855fall11TerminalVelocityConnectionString %>"
                                        SelectCommand="SELECT [eventTypeName] FROM [VIEW_EVENTTYPES]"></asp:SqlDataSource>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset>
                            <legend>Where &amp; When Would You Like To Experience Your Extreme Sport?</legend>
                            <div class="row">
                                <div class="col">
                                    <label for="ddlCity">
                                        State</label>
                                    <asp:DropDownList ID="ddlState" runat="server" DataSourceID="SqlDataSource3" DataTextField="State"
                                        DataValueField="State" AutoPostBack="True">
                                    </asp:DropDownList>
                                    <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:cop3855fall11TerminalVelocityConnectionString %>"
                                        SelectCommand="SELECT DISTINCT [State] FROM [VIEW_CITYSTATES] ORDER BY [State]">
                                    </asp:SqlDataSource>
                                </div>

                                <div class="col">
                                    <label for="ddlCity">City</label>
                                    <asp:DropDownList ID="ddlCity" runat="server" DataSourceID="SqlDataSource4" DataTextField="City"
                                        DataValueField="City" AutoPostBack="True">
                                    </asp:DropDownList>
                                    <asp:SqlDataSource ID="SqlDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:cop3855fall11TerminalVelocityConnectionString %>"
                                        SelectCommand="SELECT [City] FROM [VIEW_CITYSTATES] WHERE ([State] = @State)">
                                        <SelectParameters>
                                            <asp:ControlParameter ControlID="ddlState" Name="State" PropertyName="SelectedValue"
                                                Type="String" />
                                        </SelectParameters>
                                    </asp:SqlDataSource>
                                </div>
                                
                                <div class="col">
                                    <label for="dpBegin">
                                        Available From</label>
                                    <asp:TextBox ID="dpBegin" runat="server"></asp:TextBox>
                                </div>
                                
                                <div class="col">
                                    <label for="dpEnd">
                                        Available Until</label>
                                    <asp:TextBox ID="dpEnd" runat="server"></asp:TextBox>
                                </div>
                            </div>
                        </fieldset>

                        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:cop3855fall11TerminalVelocityConnectionString %>"
                            SelectCommand="SELECT * FROM [VIEW_EVENTS] WHERE (([Event Type] = @Event_Type) AND ([State] = @State) AND ([City] = @City))">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="ddlEventType" Name="Event_Type" PropertyName="SelectedValue"
                                    Type="String" />
                                <asp:ControlParameter ControlID="ddlState" Name="State" PropertyName="SelectedValue"
                                    Type="String" />
                                <asp:ControlParameter ControlID="ddlCity" Name="City" PropertyName="SelectedValue"
                                    Type="String" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                        
                        <fieldset>
                            <legend>Here Is What We Have Found For You...</legend>
                            <asp:Repeater ID="rpDetails" runat="server" DataSourceID="SqlDataSource2" 
                                onitemcommand="rpDetails_ItemCommand">
                                <ItemTemplate>
                                    <div class="details-box-container grey1-gradient">
                                        <div class="details-box-left">
                                            <div class="details-box-cost">
                                                <h2>
                                                    How Much Is It?</h2>
                                                <h1>
                                                    $<%# DataBinder.Eval(Container.DataItem, "Cost") %></h1>
                                            </div>
                                        </div>
                                        <div class="details-box-right">
                                            <h1>
                                                <%# DataBinder.Eval(Container.DataItem, "Event Name") %>
                                            </h1>
                                            <div class="details-box-right-contents">
                                                <p>
                                                    Location:&nbsp;
                                                    <%# DataBinder.Eval(Container.DataItem, "Street")%>,&nbsp;
                                                    <%# DataBinder.Eval(Container.DataItem, "City") %>,&nbsp;
                                                    <%# DataBinder.Eval(Container.DataItem, "State") %>
                                                </p>
                                                <p>
                                                    Event Contact:&nbsp;<%# DataBinder.Eval(Container.DataItem, "Event Contact")%></p>
                                                <p>
                                                    Available From:
                                                    <%# DataBinder.Eval(Container.DataItem, "Available") %>&nbsp; 
                                                    
                                                    Available Until:
                                                    <%# DataBinder.Eval(Container.DataItem, "Ends") %>
                                                </p>
                                            </div>
                                            <div class="details-buttons-box">
                                                <asp:Button ID="btnBook" runat="server" Text="Book This Event!" CommandName="BookEvent" CommandArgument='<%# Eval("eventID") %>' />
                                            </div>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </fieldset>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
