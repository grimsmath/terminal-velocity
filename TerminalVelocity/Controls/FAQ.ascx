﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FAQ.ascx.cs" Inherits="TerminalVelocity.Controls.FAQ" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>

<h1>Frequently Asked Questions</h1>
<asp:Accordion ID="accordionQuestions" runat="server" SelectedIndex="0"
    HeaderCssClass="accordionHeader" HeaderSelectedCssClass="accordionHeaderSelected"
    ContentCssClass="accordionContent" FadeTransitions="false" FramesPerSecond="40" 
    TransitionDuration="250" AutoSize="None" RequireOpenedPane="false" SuppressHeaderPostbacks="true">
    <Panes>
        <asp:AccordionPane ID="AccordionPane1" runat="server">
            <Header>
                <a href="#" class="accordionLink">1. I tried booking a trip, but have some questions, can you help me?</a>
            </Header>
            <Content>
                Yes we can, feel free to contact our customer support with any questions 
                you may have about your experience with Terminal Velocity.
            </Content>
        </asp:AccordionPane>
        <asp:AccordionPane ID="AccordionPane2" runat="server">
            <Header>
                <a href="#" class="accordionLink">2.  I’m having trouble signing into Terminal Velocity.</a>
            </Header>
            <Content>
                You don’t have to sign into Terminal Velocity to use our services. However, 
                if you want us to remember your travel preferences, you will need to create a 
                free login account, using your email address. If you’ve forgotten your password, 
                please click “Forgot Password?” Terminal Velocity will then send an email to your 
                email address provided where you can create a new password.
            </Content>
        </asp:AccordionPane>
        <asp:AccordionPane ID="AccordionPane3" runat="server">
            <Header>
                <a href="#" class="accordionLink">3.  How can I delete my Terminal Velocity account?</a>
            </Header>
            <Content>
                Please send us an email requesting to be removed from our records. 
                Make sure you do this from the email address you wish to be deleted.
            </Content>
        </asp:AccordionPane>
        <asp:AccordionPane ID="AccordionPane4" runat="server">
            <Header>
                <a href="#" class="accordionLink">4.  What is the cost of using your website?</a>
            </Header>
            <Content>
               Terminal Velocity is free for everyone.
            </Content>
        </asp:AccordionPane>
        <asp:AccordionPane ID="AccordionPane5" runat="server">
            <Header>
                <a href="#" class="accordionLink">5.  Is there any way to submit feedback about your website?</a>
            </Header>
            <Content>
               Yes, please go to our Feedback page to submit feedback about your experience on Terminal Velocity.
            </Content>
        </asp:AccordionPane>
        <asp:AccordionPane ID="AccordionPane6" runat="server">
            <Header>
                <a href="#" class="accordionLink">6.  I don’t want to receive emails from Terminal Velocity.</a>
            </Header>
            <Content>
               You can configure this when you sign up for a new Terminal Velocity account. However, if you 
               wish to change these settings, at any time just sign into your account, and go to the settings 
               page to change any features of our services you wish to change.
            </Content>
        </asp:AccordionPane>
        <asp:AccordionPane ID="AccordionPane7" runat="server">
            <Header>
                <a href="#" class="accordionLink">7.  Sometimes when I search for a specific area, no results appear.</a>
            </Header>
            <Content>
               At this time we are not able to provide extreme sports in every region of the country. If this
               happens, either there were no results found for that city, or we have not been able to 
               offer extreme sports in this area… yet.
            </Content>
        </asp:AccordionPane>
        <asp:AccordionPane ID="AccordionPane8" runat="server">
            <Header>
                <a href="#" class="accordionLink">8.  Your search is limited, is there any way to just book extreme sports through your business?</a>
            </Header>
            <Content>
               Yes. We are working to improve our search results for our customers, however if you just want to 
               book extreme sports activities for your trip, you can do that through us. Just search for extreme 
               sports instead of searching packages, and you should be able to book extreme sports activities 
               to suite your trip.
            </Content>
        </asp:AccordionPane>
        <asp:AccordionPane ID="AccordionPane9" runat="server">
            <Header>
                <a href="#" class="accordionLink">9.  Can I cancel a service I ordered from your webpage?</a>
            </Header>
            <Content>
               Yes, if within a respectable window of purchasing. We allow cancelations up to 2 weeks before your trip. 
               However, as your trip approaches, it is harder to allow cancelations. 
            </Content>
        </asp:AccordionPane>
        <asp:AccordionPane ID="AccordionPane10" runat="server">
            <Header>
                <a href="#" class="accordionLink">10.  Why don’t you offer extreme sports listings in every state?</a>
            </Header>
            <Content>
               Because Terminal Velocity is just starting up, we have only been able to let customers search 
               certain states and areas of the country. We plan to expand and offer extreme sports from anywhere 
               in the country.
            </Content>
        </asp:AccordionPane>
    </Panes>
</asp:Accordion>