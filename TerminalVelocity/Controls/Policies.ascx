﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Policies.ascx.cs" Inherits="TerminalVelocity.Controls.Policies" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>

<h1>Some Stuff You Need To Know...</h1>
<asp:Accordion ID="accordionPolicies" runat="server" SelectedIndex="0"
    HeaderCssClass="accordionHeader" HeaderSelectedCssClass="accordionHeaderSelected"
    ContentCssClass="accordionContent" FadeTransitions="false" FramesPerSecond="40" 
    TransitionDuration="250" AutoSize="None" RequireOpenedPane="false" SuppressHeaderPostbacks="true">
    <Panes>
        <asp:AccordionPane ID="AccordionPane4" runat="server">
            <Header><a href="#" class="accordionLink">Privacy Policy</a></Header>
            <Content>
                Content 1
            </Content>
        </asp:AccordionPane>
        <asp:AccordionPane ID="AccordionPane5" runat="server">
            <Header><a href="#" class="accordionLink">Online Access Conditions</a></Header>
            <Content>
                Content 2
            </Content>
        </asp:AccordionPane>
        <asp:AccordionPane ID="AccordionPane6" runat="server">
            <Header><a href="#" class="accordionLink">Payment Terms &amp; Conditions</a></Header>
            <Content>
                Content 3
            </Content>
        </asp:AccordionPane>
        <asp:AccordionPane ID="AccordionPane8" runat="server">
            <Header><a href="#" class="accordionLink">Copyright &amp; Trademarks</a></Header>
            <Content>
                Content 3
            </Content>
        </asp:AccordionPane>
    </Panes>
</asp:Accordion>