﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="Browse.aspx.cs" Inherits="TerminalVelocity.Browse" %>

<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ MasterType VirtualPath="~/Site.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="Server" />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="grid_24">
                <div class="center_box">
                    <fieldset>
                        <legend>What Type Of Extreme Sport Are You Interested In?</legend>
                        <div class="row">
                            <div class="col">
                                <label for="ddlEventType">
                                    Event Type</label>
                                <asp:DropDownList ID="ddlEventType" runat="server" DataTextField="eventTypeName"
                                    DataValueField="eventTypeName" DataSourceID="SqlDataSource1" AutoPostBack="True">
                                </asp:DropDownList>
                                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:cop3855fall11TerminalVelocityConnectionString %>"
                                    SelectCommand="SELECT DISTINCT [eventTypeName] FROM [VIEW_EVENTTYPES] ORDER BY [eventTypeName]">
                                </asp:SqlDataSource>
                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                        <legend>Here Is What We Have Found For You...</legend>
                        <asp:Repeater ID="rpDetails" runat="server" DataSourceID="SqlDataSource2" 
                            onitemcommand="rpDetails_ItemCommand">
                            <ItemTemplate>
                                <div class="details-box-container grey1-gradient">
                                    <div class="details-box-left">
                                        <div class="details-box-cost">
                                            <h2>
                                                How Much Is It?</h2>
                                            <h1>
                                                $<%# DataBinder.Eval(Container.DataItem, "Cost") %></h1>
                                        </div>
                                    </div>
                                    <div class="details-box-right">
                                        <h1>
                                            <%# DataBinder.Eval(Container.DataItem, "Event Name") %>
                                        </h1>
                                        <div class="details-box-right-contents">
                                            <h3>Location:&nbsp;
                                                <%# DataBinder.Eval(Container.DataItem, "Street")%>,&nbsp;
                                                <%# DataBinder.Eval(Container.DataItem, "City") %>,&nbsp;
                                                <%# DataBinder.Eval(Container.DataItem, "State") %>
                                            </h3>
                                            <h4>
                                                Event Contact:&nbsp;<%# DataBinder.Eval(Container.DataItem, "Event Contact")%></h4>
                                            <h4>
                                                Available From:
                                                <%# DataBinder.Eval(Container.DataItem, "Available") %>&nbsp; 
                                                
                                                Available Until:
                                                <%# DataBinder.Eval(Container.DataItem, "Ends") %>
                                            </h4>
                                        </div>
                                        <div class="details-buttons-box">
                                            <asp:Button ID="btnBook" runat="server" Text="Book This Event!" CommandName="BookEvent" CommandArgument='<%# Eval("eventID") %>' />
                                        </div>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:cop3855fall11TerminalVelocityConnectionString %>"
                            SelectCommand="SELECT * FROM [VIEW_EVENTS] WHERE ([Event Type] = @Event_Type) ORDER BY [State], [City]">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="ddlEventType" Name="Event_Type" PropertyName="SelectedValue"
                                    Type="String" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </fieldset>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
