﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TerminalVelocity
{
	public partial class _Default : System.Web.UI.Page
	{
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Master.SetActiveTab("lnkMenuSearch");
        }

        protected void rpDetails_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "BookEvent")
            {
                if (Session["username"] != null && Session["username"].ToString() != "")
                {
                    String eventID = e.CommandArgument.ToString();
                    Response.Redirect("~/Account/Cart.aspx?action=BookEvent&eventID=" + eventID);
                }
                else
                {
                    Response.Redirect("~/Account/Login.aspx");
                }
            }
        }
	}
}
