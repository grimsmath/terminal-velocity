﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="Contact.aspx.cs" Inherits="TerminalVelocity.Feedback" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="container_24">
                <div class="grid_5">
                    &nbsp;
                </div>
                <div class="grid_14">
                    <div class="center_box">
                        <fieldset>
                            <legend>Let Us Know About Your Recent Booking</legend>
                            <table border="0" width="100%">
                                <tr>
                                    <td>Date of your trip:</td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:PlaceHolder ID="PlaceHolder1" runat="server">
                                        <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="tbxDate">
                                        </asp:CalendarExtender>
                                        <asp:TextBox ID="tbxDate" runat="server"></asp:TextBox>
                                        
                                        </asp:PlaceHolder>
                                    </td>                                    
                                </tr>
                                <tr>
                                    <td>
                                        Tell us about your trip:
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:TextBox ID="tbxFeedback" runat="server" CssClass="textarea" 
                                            TextMode="MultiLine"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Button ID="btnSubmitFeedback" runat="server" Text="Submit" 
                                            CssClass="submit-button-left" />
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </div>
                </div>
                <div class="grid_5">
                    &nbsp;
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
