USE [cop3855fall11TerminalVelocity]
GO

/****** Object:  StoredProcedure [dbo].[BookEvent]    Script Date: 12/02/2011 04:36:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SubmitPayment]
	@customerID int,
    @cardType varchar(50),
    @cardNumber varchar(50),
    @expireMonth int,
    @expireYear int,
    @securityCode varchar(10)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @payID INT;
	
	INSERT INTO
		dbo.tv_Payment
	VALUES
		(@customerID, @cardType, @cardNumber, @expireMonth, @expireYear, @securityCode);			
		
	SET @payID = @@IDENTITY;
END

GO


