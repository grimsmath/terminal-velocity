USE [cop3855fall11TerminalVelocity]
GO

/****** Object:  View [dbo].[VIEW_BOOKINGS]    Script Date: 12/02/2011 01:30:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[VIEW_OPEN_BOOKINGS]
AS
SELECT     
	dbo.tv_Booking.bookingID, 
	dbo.tv_Booking.customerID,
	dbo.tv_Event.eventID,
	dbo.tv_Event.eventName,
	dbo.tv_Booking.paymentID,
	dbo.tv_Address.street,
	dbo.tv_City.cityName,
	dbo.tv_City.stateCode,
	dbo.tv_Event.eventCost
FROM         
	dbo.tv_Booking
WHERE
	dbo.tv_Booking.bookingStatus = 0
INNER JOIN
    dbo.tv_Booking_Event ON dbo.tv_Booking.bookingID = dbo.tv_Booking_Event.bookingID 
INNER JOIN
	dbo.tv_Customer ON dbo.tv_Booking.customerID = dbo.tv_Customer.customerID
INNER JOIN
	dbo.tv_Event ON dbo.tv_Booking_Event.eventID = dbo.tv_Event.eventID
INNER JOIN
	dbo.tv_Address ON dbo.tv_Event.eventAddressID = dbo.tv_Address.addressID
INNER JOIN
	dbo.tv_City ON dbo.tv_City.cityID = dbo.tv_Address.cityID
GO


