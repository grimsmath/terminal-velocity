-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE dbo.RegisterCustomer
	@FirstName nvarchar(50), 
    @LastName nvarchar(50),
    @Email nvarchar(255),
    @Phone nvarchar(50),
    @Street nvarchar(50),
    @City nvarchar(50),
    @State nvarchar(50),
    @Postal nvarchar(50),
    @Username nvarchar(50),
    @Password nvarchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @cityID AS int;
	SET @cityID = (SELECT dbo.tv_City.cityID FROM dbo.tv_City 
		WHERE dbo.tv_City.cityName = @City AND dbo.tv_City.stateCode = @State);
	
    -- Insert into the customer table
	INSERT INTO dbo.tv_Customer
		VALUES (@FirstName, @LastName, @Phone, @Email);

	-- Insert into the address table
	INSERT INTO dbo.tv_Address
		VALUES (@Street, @cityID, @Postal); 
		
	-- Insert into the User table
	INSERT INTO dbo.tv_User
		VALUES (@Username, @Password, @FirstName, @LastName, 1);
END
GO
